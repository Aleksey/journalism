# NordVPN and The Young Turks

Supporting files for report on The Young Turks carelessly sponsoring NordVPN.

Article: [https://medium.com/@EpsilonCalculus/when-shoddy-journalists-sponsor-shoddy-cybersecurity-services-60635a6aa56d](https://medium.com/@EpsilonCalculus/when-shoddy-journalists-sponsor-shoddy-cybersecurity-services-60635a6aa56d)

* NOTE: Base Image for Vaporwave'd Icon: https://www.cisco.com/c/en/us/td/docs/security/security_management/cisco_security_manager/security_manager/3-3/user/guide/CSMUserGuide_wrapper/vpchap.html

## Videos

|ID|Video name|Original link|Archive.today link|Video Mirror|
|--|----------|-------------|------------------|------------|
| 1. | Xenophobic Senator MELTS DOWN on Fox News | [https://www.youtube.com/watch?v=BkYB0NlIEOs​](https://www.youtube.com/watch?v=BkYB0NlIEOs) | [https://archive.today/jZSJG](https://archive.today/jZSJG) | todo* |
| 2. | Trump Tower BUSTED With Criminals | [https://www.youtube.com/watch?v=ajA1RM-VEbM​](https://www.youtube.com/watch?v=ajA1RM-VEbM) | [https://archive.today/ZNh2R](https://archive.today/ZNh2R) | todo* |
| 3. | New York Times... Shame, Shame, Shame | [https://www.youtube.com/watch?v=dzidBFMMCE8​](https://www.youtube.com/watch?v=dzidBFMMCE8) | [https://archive.today/E00Gq](https://archive.today/E00Gq) | todo* |
| 4. | How To Get Super Powers | [https://www.youtube.com/watch?v=rZe-AYboX2o​](https://www.youtube.com/watch?v=rZe-AYboX2o) | [https://archive.today/9w9BN](https://archive.today/9w9BN) | todo* |
| 5. | Cenk Goes Off On NYPD and Eric Garner's Killer | [https://www.youtube.com/watch?v=yyWhpU7ls9g​](https://www.youtube.com/watch?v=yyWhpU7ls9g) | [https://archive.today/O5mMO](https://archive.today/O5mMO) | todo* |
| 6. | Establishment PANICS Over Iowa Chaos | [https://www.youtube.com/watch?v=v8W855krICw](https://www.youtube.com/watch?v=v8W855krICw) | [https://archive.today/KOIbc](https://archive.today/KOIbc)​ | todo* |
| 7. | Biden RIPS Bloomberg With New Ad | [https://www.youtube.com/watch?v=KPpWR1QmWRI](https://www.youtube.com/watch?v=KPpWR1QmWRI) | [https://archive.today/jSsfB](https://archive.today/jSsfB)​ | todo* |
| 8. | Top 10 Bloomberg Facts You Need To Know | [https://www.youtube.com/watch?v=ZJmH0MzlmTk](https://www.youtube.com/watch?v=ZJmH0MzlmTk) | [https://archive.today/RcOu8](https://archive.ph/RcOu8) | todo* |
| 9. | Hannity’s Bernie Sanders Confession | [https://www.youtube.com/watch?v=nI_YgUjjWOo](https://www.youtube.com/watch?v=nI_YgUjjWOo) | [https://archive.today/TljUR​](https://archive.today/TljUR) | todo* |
| 10. | Angry Trump Calls Kellyanne Conway's Husband A Stone Cold Loser | [https://www.youtube.com/watch?v=gJQ9_01ZsKA](https://www.youtube.com/watch?v=gJQ9_01ZsKA) | [https://archive.today/J32jA](https://archive.today/J32jA) | todo* |
| 11. | Trump Cries About Poll Numbers As America Implodes | [https://www.youtube.com/watch?v=_I6cnjoGSU0](https://www.youtube.com/watch?v=_I6cnjoGSU0)​ | [https://archive.today/vJJK8](https://archive.today/vJJK8) | todo* |
| 12. | Irate Trump Is About To EXPLODE On His Staff | [https://www.youtube.com/watch?v=iE4RABQJ-qU](https://www.youtube.com/watch?v=iE4RABQJ-qU) | [https://archive.today/lQ2QI](https://archive.today/lQ2QI) | todo* |
| 13. | Mike Pence Gives Insane Defense For Not Wearing Mask At Mayo Clinic | [https://www.youtube.com/watch?v=RodGYcue_Lk](https://www.youtube.com/watch?v=RodGYcue_Lk) | [https://archive.today/WNo6Y](https://archive.today/WNo6Y) | todo* |

## Other pages

* TYT Facebook Post That Promotes NordVPN: https://archive.today/HoHat
